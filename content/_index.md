+++
title = "Jochum van der Ploeg"
+++

# About me

{{< figure class="avatar" src="/avatar.jpeg" >}}

Hello there, my name is Jochum{{< pronunciation what="jochum" >}} van der Ploeg, also known on the internet as [wolfenra.in](https://wolfenra.in/).

As a human<sup>[**citation needed**]</sup> being who loves [Dungeons & Dragons](https://dnd.wizards.com/), [Magic the Gathering](https://magic.wizards.com/en) and many other fantasy related subjects I am often considered a geek or a nerd. To make matters worse, I also love to tinker and mess with things that I probably should not touch... but it does complete the stereotype.

When I'm not playing make believe or hacking and slashing in someone else's code, I also conquer professional challenges. Working on those I consider myself as someone who thinks outside the box and I challenge myself to grow whenever the opportunity allows for it. 

One type of challenge that I like to give myself is to work on open source projects, this allows me to learn new aspects of programming and gives me the opportunity to grow into a community. 

Even though I love working on open source projects, I find the most interesting challenges in game development. I love to make use of experimental rendering techniques and I often read up on the ins and outs of any interesting technique I’ve found before I go to bed.

These interests of mine are intertwined with my core strengths:
- I am analytical.
- I am constructive.
- I am dedicated.
- I am open to learn and adapt.

I am an advocate of open source software, and when a person or a company ask my opinion about it I always say the same thing:
> You should give back to the open source community by either contributing, interacting with the community or open source your own projects. You will benefit from it in the long run.

# Work Experience

{{< experience 
    from="Mar 2022" 
    to="Present" 
    title="Open Source Engineer"
    description="App development consultancy company that designs and build Flutter applications. <br><br>Working as an open source engineer, maintaining open source packages from Very Good Ventures and packages that they use interally."
    company="Very Good Ventures"
    companyLink="https://verygood.ventures/"

    task1="Maintaining existing open source packages either from Very Good Ventures or external ones. Including triaging issues, bug fixes, feature implementations and improving documentation."
    task2="Writing blog posts and speak at events about various open source projects and toolings."
    task3="Represent Very Good Venture at meetups and conferences."

    tech1="[Flutter](https://flutter.dev)"
    tech2="[Dart](https://dart.dev)"
>}}

{{< experience 
    from="Mar 2018" 
    to="Feb 2022" 
    title="Full Stack Engineer"
    description="Startup company focused on video call solutions for the healthcare sector. <br><br>Working as a senior engineer and as a team lead for a team of six. Keeping close communication with both the product owner and the stakeholders of different projects."
    company="Mobiléa"
    companyLink="https://mobilea.nl/"

    task1="Building custom made solutions for customers that integrate with the Mobiléa platform."
    task2="Maintaining the open source Flutter plugins [twilio_programmable_video](https://gitlab.com/twilio-flutter/programmable-video) and [twilio_programmable_chat](https://gitlab.com/twilio-flutter/programmable-chat)."
    task3="Leading a small team on multiple internal projects."
    task4="Providing technical guidance for multiple internal projects."
    task5="Fulfilling the role of Scrum Master."

    tech1="[Flutter](https://flutter.dev)"
    tech2="[Dart](https://dart.dev)"
    tech3="[NodeJS](https://nodejs.org)"
    tech4="[TypeScript](https://www.typescriptlang.org/) and by extension [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript)"
    tech5="[Kotlin](https://kotlinlang.org)"
    tech6="[Swift](https://developer.apple.com/swift/)"
    tech7="[GitLab](https://gitlab.com)"
>}}

---

{{< experience 
    from="Sep 2017" 
    to="Mar 2018" 
    title="Web Development"
    description="Freelancing for multiple companies, building custom-made websites and systems. Together with [Kyzoe Hosting & Design](https://kyzoe.hosting)."
    company="Freelancing"

    project1="Created a backend management system for servers."
    project1="Created a server monitoring tool for CentOS servers."

    tech1="PHP"
    tech2="Apache"
    tech3="MySQL"
>}}

# Education

{{< experience 
    from="Aug 2018" 
    to="Present" 
    title="Bachelor of Computer Science"
    description="[Windesheim - Zwolle, Netherlands](https://www.windesheim.nl/)"

    item1="_Major_: Software engineering"
    item2="_Minor_: Data science"
    item3="Part-time study"
    item4="Expected graduation date: August 2022"
>}}

---

{{< experience 
    from="Aug 2015" 
    to="July 2018" 
    title="Computer Software and Media Applications"
    description="[Friesland College - Heerenveen - Netherlands](https://www.frieslandcollege.nl/)"
    
    item1="Graduated: July 2018"
>}}   


# Open source 

{{< experience 
    from="Dec 2020" 
    to="Present" 
    title="Maintainer"
    description="Maintaining and developing packages for Flutter under the Blue Fire organization."
    company="Blue Fire"
    companyLink="https://github.com/bluefireteam/"

    project1="[Flame Engine](https://flame-engine.org), a 2D game engine built on top of Flutter."
    project2="[Oxygen](https://pub.dev/packages/oxygen), an ECS framework for the Dart language."
    project3="And many more, see the link above for all the projects."

    task1="Developing and maintaining the sourcecode."
    task2="Handling bug reports and feature requests."
    task3="Reviewing pull requests from contributors."
    task4="Manage the Discord community."

    tech1="[Flutter](https://flutter.dev)"
    tech2="[Dart](https://dart.dev)"
>}}

---

{{< experience 
    from="May 2020" 
    to="Present" 
    title="Maintainer"
    description="Maintaining and developing Flutter plugins for the [Twilio Programmable Video](https://www.twilio.com/video?utm_source=opensource&utm_campaign=flutter-plugin) and the [Twilio Programmable Chat](https://www.twilio.com/chat?utm_source=opensource&utm_campaign=flutter-plugin) SDKs."
    company="twilio-flutter"
    companyLink="https://gitlab.com/twilio-flutter"

    project1="[twilio_programmable_video](https://pub.dev/packages/twilio_programmable_video), brings the Programmable Video SDK to Flutter."
    project2="[twilio_programmable_chat](https://pub.dev/packages/twilio_programmable_chat), brings the Programmable Chat SDK to Flutter."
    project2="[twilio_conversations](https://gitlab.com/twilio-flutter/conversations), brings the Conversations SDK to Flutter. Currently under development."

    task1="Developing and maintaining the sourcecode."
    task2="Handling bug reports and feature requests."
    task3="Reviewing merge requests from contributors."
    task4="Manage the Discord community."
    task4="Keep close contact with Twilio for ease of communication."

    tech1="[Flutter](https://flutter.dev)"
    tech2="[Dart](https://dart.dev)"
    tech3="[Kotlin](https://kotlinlang.org)"
    tech4="[Swift](https://developer.apple.com/swift/)"
>}}

---

{{< experience 
    from="May 2021" 
    to="Present" 
    title="Maintainer"
    description="A personal experimental pet project that adds multi window support to Flutter on Desktop."
    company="multi_window"
    companyLink="https://pub.dev/packages/multi_window"

    tech1="[C++](https://en.wikipedia.org/wiki/C%2B%2B)"
    tech2="[GTK](https://www.gtk.org/)"
    tech3="[Swift](https://developer.apple.com/swift/)"
>}}

---

{{< experience 
    from="Oct 2020" 
    to="Present" 
    title="Maintainer"
    description="A Dart package that brings Kotlin-like features to Dart. "
    company="dartlin"
    companyLink="https://gitlab.com/wolfenrain/dartlin"

    task1="Developing and maintaining the sourcode."
    task2="Handling bug reports and feature requests."
    task3="Reviewing merge requests from contributors."

    tech1="[Flutter](https://flutter.dev)"
    tech2="[Dart](https://dart.dev)"
>}}

## Small but not insignificant projects
- [Binarize](https://pub.dev/packages/binarize) is a package for the Dart language that wraps the ByteData functionality into a more streamlined and extendable payload system.
- [Flavor Text](https://pub.dev/packages/flavor_text) is a lightweight and fully customisable text parser for Flutter.
- [Benchmark](https://pub.dev/packages/benchmark) provides a standard way of writing and running benchmarks for the Dart language.
- [dart-raylib](https://pub.dev/packages/raylib) is a Dart package that brings bindings for [raylib](https://raylib.com) to Dart.

# Skillset

{{< experience 
    from="Programming Languages"
    itemTitle="These are all the languages I have worked with or am interested in"
    item1="Highly proficient with: [Dart](https://dart.dev/), [TypeScript](https://www.typescriptlang.org/), [JavaScript](https://developer.mozilla.org/en-US/docs/Web/JavaScript)"
    item2="Proficient with: [Kotlin](https://kotlinlang.org/), [Swift](https://developer.apple.com/swift/), [Java](https://java.com/), [C#](https://docs.microsoft.com/en-us/dotnet/csharp/)"
    item3="Familiar with: [Objective-C](https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/ProgrammingWithObjectiveC/Introduction/Introduction.html), [POSIX Shell Command Language](https://pubs.opengroup.org/onlinepubs/9699919799/utilities/V3_chap02.html), [C++](https://en.wikipedia.org/wiki/C%2B%2B)"
>}}

{{< experience 
    from="Frameworks"
    itemTitle="These are the frameworks that I have worked with in the past"
    item1="[Flutter](https://flutter.dev/)"
    item2="[NestJS](https://nestjs.com/)"
    item3="[Angular](https://angular.io/)"
    item4="[Express](https://expressjs.com/)"
>}}

{{< experience 
    from="Others"
    itemTitle="Other skill that I possess that does not fit within any kind of category"
    item1="Git"
    item2="GitLab CI/CD"
    item3="Docker"
    item4="Game development"
>}}

### Languages

| Language | Skill level |
| -------- | ----------- |
| English  | [C1](https://en.wikipedia.org/wiki/Common_European_Framework_of_Reference_for_Languages) |
| Dutch    | Native      |
| Frisian  | Native      |

### Random facts about me

- I own a working Commodore 64 that is older than I am.
- I accidentally started a rubber duck collection.
- I have a growing collection of unique and strange Linux devices.